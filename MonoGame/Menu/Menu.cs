﻿using GeonBit.UI;
using GeonBit.UI.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGame
{
    public class Menu : MenuBase
    {
        //FIELDS
        private Sprite logo;

        private Button startButton;
        private Button optionsButton;
        private Button quitButton;
        private Button aboutButton;

        private Scale scaleElement = new Scale();
        private PlaySound playSound;

        //CONSTRUCTOR
        public Menu() : base()
        {
            float scaleValue = (float)scaleElement.GetScale();
            UserInterface.Active.GlobalScale = scaleValue;
            UserInterface.Active.CursorScale = scaleValue;
            UserInterface.Active.ShowCursor = true;

            this.logo = new Sprite(Resources.Images["logo"], Settings.ScreenWidth / 2 - 300, 50);
            startButton = new Button(Text.new_game, ButtonSkin.Default, Anchor.Center, size: new Vector2(scaleElement.TextWidth(Text.save, 0) / scaleValue, scaleElement.TextHeight(Text.save, 0) / scaleValue), offset: new Vector2(0, -100 / scaleElement.GetScaleY())); //
            optionsButton = new Button(Text.options, ButtonSkin.Default, Anchor.Center, size: new Vector2(scaleElement.TextWidth(Text.save, 0) / scaleValue, scaleElement.TextHeight(Text.save, 0) / scaleValue), offset: new Vector2(0, 0 / scaleElement.GetScaleY()));
            quitButton = new Button(Text.exit, ButtonSkin.Default, Anchor.Center, size: new Vector2(scaleElement.TextWidth(Text.save, 0) / scaleValue, scaleElement.TextHeight(Text.save, 0) / scaleValue), offset: new Vector2(0, 200 / scaleElement.GetScaleY()));
            aboutButton = new Button(Text.about, ButtonSkin.Default, Anchor.Center, size: new Vector2(scaleElement.TextWidth(Text.save, 0) / scaleValue, scaleElement.TextHeight(Text.save, 0) / scaleValue), offset: new Vector2(0, 100 / scaleElement.GetScaleY()));
            UserInterface.Active.AddEntity(aboutButton);
            UserInterface.Active.AddEntity(startButton);
            UserInterface.Active.AddEntity(optionsButton);
            UserInterface.Active.AddEntity(quitButton);
        }

        //UPDATE & DRAW
        public override void Update(GameTime gameTime, Input input, Game1 game)
        {
            base.Update(gameTime, input, game);

            startButton.OnClick = (Entity btn) =>
            {
                playSound = new PlaySound("click", (float)Settings.effectVolume * (float)Settings.masterVolume);
                game.ChangeScene(Scene.CORE, "");
                DeleteAllElements();
            };

            aboutButton.OnClick = (Entity btn) =>
            {
                playSound = new PlaySound("click", (float)Settings.effectVolume * (float)Settings.masterVolume);
                game.ChangeScene(Scene.ABOUT, "");
                DeleteAllElements();
            };

            quitButton.OnClick = (Entity btn) =>
            {
                playSound = new PlaySound("click", (float)Settings.effectVolume * (float)Settings.masterVolume);
                game.Exit();
            };

            optionsButton.OnClick = (Entity btn) =>
            {
                playSound = new PlaySound("click", (float)Settings.effectVolume * (float)Settings.masterVolume);
                game.ChangeScene(Scene.OPTIONS, "");
                DeleteAllElements();
            };

        }
        public override void Draw(SpriteBatch spriteBatch)
        {

            base.Draw(spriteBatch);
            spriteBatch.Draw(Resources.Images["background"], new Rectangle(0, 0, Settings.ScreenWidth, Settings.ScreenHeight), Color.White);
            this.logo.Draw(spriteBatch);
        }

        private void DeleteAllElements()
        {
            quitButton.Visible = false;
            startButton.Visible = false;
            optionsButton.Visible = false;
            aboutButton.Visible = false;
        }
    }
}
