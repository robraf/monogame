﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Windows.Forms;


namespace MonoGame
{
    public class LoadSettings
    {
        //DEFAULT VALUES
        public static string language = "en";
        public int resolutionX;
        public int resolutionY;
        public static bool fullscreen = false;
        public static float soundmaster = 0.55f;
        public static float soundmusic = 0.15f;
        public static float soundeffect = 1f;

        //FIELDS
        private XmlTextReader reader;
        private XmlWriterSettings localSettings;
        private ArrayList loadedConfigList = new ArrayList();
        private string path = @"config.xml";

        //Write here all values exsisting in config.xml - it's necessary to check correction of file
        private string[] configValueTable = new string[] { "fullscreen", "language", "soundmaster", "soundmusic", "soundeffect", "resolutionX", "resolutionY" };

        //METHODS  

        public void Load(int resX, int resY)
        {
            this.resolutionX = resX;
            this.resolutionY = resY;
        }

        public void CheckExtistingOfFile()
        {
            if (!File.Exists(path))
            {
                CreateNewFile();
            }
            try
            {
                this.reader = new XmlTextReader(path);
                GetValues();
                CheckCorrectionOfFile();
            }
            catch
            {             
                CreateNewFile();
                MessageBox.Show("Found some errors in 'config.xml'. Default settings restored, run appliaction again.", "Error" , MessageBoxButtons.OK, MessageBoxIcon.Warning);
                System.Environment.Exit(1);
            }
        
        }


        private void CheckCorrectionOfFile()
        {
            int counterFailed = 0;
            for (int i = 0; i < configValueTable.Length; i++)
            {
                counterFailed = 0;
                foreach (string element in loadedConfigList)
                {
                    if (!configValueTable[0].Equals(element))
                    {
                        counterFailed++;
                    }
                }
              
                if (counterFailed > configValueTable.Length - 1 || loadedConfigList.Count!= configValueTable.Length)
                {
                    CreateNewFile();
                    MessageBox.Show("Found some errors in 'config.xml'. Default settings restored, run appliaction again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    System.Environment.Exit(1);
                    break;
                }
            }
        }
   
        public void CreateNewFile()
        {
            localSettings = new XmlWriterSettings();
            localSettings.WriteEndDocumentOnClose = true;
            using (XmlWriter writer = XmlWriter.Create(path))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("config");
                writer.WriteAttributeString("language", language);
                writer.WriteAttributeString("resolutionX", Settings.StartedScreenWidth.ToString());
                writer.WriteAttributeString("resolutionY", Settings.StartedScreenHeight.ToString());
                writer.WriteAttributeString("fullscreen", fullscreen.ToString());
                writer.WriteAttributeString("soundmaster", soundmaster.ToString());
                writer.WriteAttributeString("soundmusic", soundmusic.ToString());
                writer.WriteAttributeString("soundeffect", soundeffect.ToString());
                writer.Close();
            }
        }

        //this metod saving current values to xml config
        public void SaveToFile()
        {
            localSettings = new XmlWriterSettings();
            localSettings.WriteEndDocumentOnClose = true;
            using (XmlWriter writer = XmlWriter.Create(path))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("config");
                writer.WriteAttributeString("language", Settings.language);
                writer.WriteAttributeString("resolutionX", Settings.ScreenWidth.ToString());
                writer.WriteAttributeString("resolutionY", Settings.ScreenHeight.ToString());
                writer.WriteAttributeString("fullscreen", Settings.Fullscreen.ToString());
                writer.WriteAttributeString("soundmaster", Settings.masterVolume.ToString());
                writer.WriteAttributeString("soundmusic", Settings.musicVolume.ToString());
                writer.WriteAttributeString("soundeffect", Settings.effectVolume.ToString());
                writer.Close();
            }
        }

        public void SetDefaultSettings()
        {
            Settings.language = language;
            Settings.ScreenWidth = resolutionX;
            Console.WriteLine(resolutionX + "x" + resolutionY);
            Settings.ScreenHeight = resolutionY;
            Settings.Fullscreen = fullscreen;
            Settings.masterVolume = soundmaster;
            Settings.musicVolume = soundmusic;
            Settings.effectVolume = soundeffect;
        }

        private void CheckValueReader(string value)
        {
            if (value.Equals(""))
            {
                reader.Close();
                CreateNewFile();
                GetValues();
            }
        }

        private void GetValues()
        {
            while (reader.Read())
            {
                while (reader.MoveToNextAttribute())
                {
                    switch (reader.Name)
                    {
                        case "language":
                            CheckValueReader(reader.Value);
                            Settings.language = reader.Value;
                            loadedConfigList.Add(reader.Name);
                            break;
                        case "resolutionX":
                            CheckValueReader(reader.Value);
                            Settings.ScreenWidth = Int32.Parse(reader.Value);
                            loadedConfigList.Add(reader.Name);
                            break;
                        case "resolutionY":
                            CheckValueReader(reader.Value);
                            Settings.ScreenHeight = Int32.Parse(reader.Value);
                            loadedConfigList.Add(reader.Name);
                            break;
                        case "fullscreen":
                            CheckValueReader(reader.Value);
                            Settings.Fullscreen = Convert.ToBoolean(reader.Value);
                            loadedConfigList.Add(reader.Name);
                            break;
                        case "soundmaster":
                            CheckValueReader(reader.Value);
                            Settings.masterVolume = float.Parse(reader.Value); ;
                            loadedConfigList.Add(reader.Name);
                            break;
                        case "soundmusic":
                            CheckValueReader(reader.Value);
                            Settings.musicVolume = float.Parse(reader.Value);
                            loadedConfigList.Add(reader.Name);
                            break;
                        case "soundeffect":
                            CheckValueReader(reader.Value);
                            Settings.effectVolume = float.Parse(reader.Value);
                            loadedConfigList.Add(reader.Name);
                            break;
                    }
                }
            }
            reader.Close();
        }
    }
}