﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGame
{
    public static class Text
    {
        public static string new_game;
        public static string exit;
        public static string save;
        public static string options;
        public static string language_select;
        public static string resolution;
        public static string full_screen;
        public static string back;
        public static string select;
        public static string sound_effect;
        public static string sound_music;
        public static string sound_master;
        public static string audio;
        public static string video;
        public static string control;
        public static string other;
        public static string default_value;
        public static string alert;
        public static string default_alert;
        public static string yes;
        public static string no;
        public static string about;
        public static string press_space_to_continue;
        public static string inventory;
        public static string inventory_empty;
        public static string resume;
        public static string back_to_menu;
        public static string exit_game;
        public static string press_e;
        public static string end_game;
        public static string pause_time;
        public static string game_time;
        public static string total_time;
        public static string chest_opened;
        public static string item_total;

    }
}
