﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace MonoGame
{

    public class ReadXml
    {
        //FIELDS
        private XmlTextReader reader;
        private XmlTextReader readerLang;
        private LoadSettings loadSettings = new LoadSettings();

        private Dictionary<string, string> languageList = new Dictionary<string, string>();

        //CONSTRUCTOR
        public ReadXml()
        {
            ReloadXmlFile();
            GetFileNameFromXml();
        }

        //METHODS   
        public void SaveXmlContent()
        {        
            while (reader.Read())
            {
                while (reader.MoveToNextAttribute())
                {
                    switch (reader.Name)
                    {
                        case "new_game":
                            Text.new_game = reader.Value;
                            break;
                        case "exit":
                            Text.exit = reader.Value;
                            break;
                        case "save":
                            Text.save = reader.Value;
                            break;
                        case "options":
                            Text.options = reader.Value;
                            break;
                        case "language_select":
                            Text.language_select = reader.Value;
                            break;
                        case "resolution":
                            Text.resolution = reader.Value;
                            break;
                        case "full_screen":
                            Text.full_screen = reader.Value;
                            break;
                        case "back":
                            Text.back = reader.Value;
                            break;
                        case "select":
                            Text.select = reader.Value;
                            break;
                        case "sound_master":
                            Text.sound_master = reader.Value;
                            break;
                        case "sound_music":
                            Text.sound_music = reader.Value;
                            break;
                        case "sound_effect":
                            Text.sound_effect = reader.Value;
                            break;
                        case "audio":
                            Text.audio = reader.Value;
                            break;
                        case "video":
                            Text.video = reader.Value;
                            break;
                        case "control":
                            Text.control = reader.Value;
                            break;
                        case "other":
                            Text.other = reader.Value;
                            break;
                        case "default_value":
                            Text.default_value = reader.Value;
                            break;
                        case "default_alert":
                            Text.default_alert = reader.Value;
                            break;
                        case "alert":
                            Text.alert = reader.Value;
                            break;
                        case "yes":
                            Text.yes = reader.Value;
                            break;
                        case "no":
                            Text.no = reader.Value;
                            break;
                        case "about":
                            Text.about = reader.Value;
                            break;
                        case "press_space_to_continue":
                            Text.press_space_to_continue = reader.Value;
                            break;
                        case "inventory":
                            Text.inventory = reader.Value;
                            break;
                        case "inventory_empty":
                            Text.inventory_empty = reader.Value;
                            break;
                        case "resume":
                            Text.resume = reader.Value;
                            break;
                        case "back_to_menu":
                            Text.back_to_menu = reader.Value;
                            break;
                        case "exit_game":
                            Text.exit_game = reader.Value;
                            break;
                        case "press_e":
                            Text.press_e = reader.Value;
                            break;
                        case "end_game":
                            Text.end_game = reader.Value;
                            break;
                        case "pause_time":
                            Text.pause_time = reader.Value;
                            break;
                        case "game_time":
                            Text.game_time = reader.Value;
                            break;
                        case "total_time":
                            Text.total_time = reader.Value;
                            break;
                        case "chest_opened":
                            Text.chest_opened = reader.Value;
                            break;
                        case "item_total":
                            Text.item_total = reader.Value;
                            break;
                    }
                }
            }
        }

        public void ReloadXmlFile()
        {          
            try
            {               
                reader = new XmlTextReader(Settings.pathLanguage + Settings.language + ".xml");               
            }
            catch
            {           
                loadSettings.CreateNewFile();
            }
           
        }

        private void GetFileNameFromXml()
        {
            foreach (var file in Directory.GetFiles(Settings.pathLanguage, "*.xml"))
            {         
                readerLang = new XmlTextReader(Settings.pathLanguage + Path.GetFileName(file));
                while (readerLang.Read())
                {
                    while (readerLang.MoveToNextAttribute())
                    {
                        if (readerLang.Name.Equals("lang"))
                        {
                            languageList.Add(Path.GetFileName(file).Split('.')[0],readerLang.Value);
                            break;
                        }
                    }
                }
            }
        }

        public Dictionary<string, string> GetLanguageFileList()
        {
            return languageList;
        }
    }
}
