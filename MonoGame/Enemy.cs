﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGame
{
    public enum EnemyType
    {
        PIG,
        SKELETON
    }

    public class Enemy : GameObject
    {
        //FIELDS
        private bool hostile;
        private int healthPoint;
        private int damage;
        private EnemyType type;
        private bool flip;

        private bool jumped;
        private bool gravity;
        public float speed;
        public float jump;
        private int timer;

        private Vector2 enemyToPlayer;
        private Core core;
        private PlaySound enemySound;

        //GETTERS & SETTERS
        public int getHP()
        {
            return this.healthPoint;
        }

        public void hit(int damage)
        {
            this.healthPoint -= damage;
            this.enemySound = new PlaySound("pig", (float)(Settings.effectVolume * Settings.masterVolume));
        }

        public EnemyType getEnemyType()
        {
            return this.type;
        }

        //CONSTRUCTOR
        public Enemy(int x, int y, EnemyType type, Core core) : base(x, y, type) //hostile true - wrogo nastawiony, false - przyjazno
        {
            switch (type)
            {
                case EnemyType.PIG:
                    this.hostile = false;
                    this.healthPoint = 10;
                    this.damage = 0;
                    this.speed = 1.5f;
                    break;
                case EnemyType.SKELETON:
                    this.hostile = true;
                    this.healthPoint = 20;
                    this.damage = 2;
                    this.speed = 3f;
                    break;
            }

            this.core = core;

            this.flip = false;
            this.jumped = false;
            this.gravity = true;
        }

        //METHODS
        public void SetGravity(bool gravity)
        {
            this.gravity = gravity;
        }

        public void SetSpeed(float speed)
        {
            this.speed = speed;
        }

        public void MovePosition(int x, int y)
        {
            this.destRectangle.Y += y;
            this.destRectangle.X += x;
        }

        //UPDATE & DRAW
        public override void Update(GameTime gameTime, Input input, Game1 game)
        {
            //SPRAWDZANIE ODLEGLOSC GRACZA OD NPC
            if (this.destRectangle.X >= (core.player.destRectangle.X - Settings.ScreenWidth / 2 - 200)
                    && this.destRectangle.X <= (core.player.destRectangle.X + Settings.ScreenWidth / 2 + 200)
                    && this.destRectangle.Y <= (core.player.destRectangle.Y + Settings.ScreenHeight / 2 + 200)
                    && this.destRectangle.Y >= (core.player.destRectangle.Y - Settings.ScreenHeight / 2 - 200))
            {
                //GRAVITY
                if (gravity)
                {
                    this.destRectangle.Y += 3;
                }

                //POSITION
                this.destRectangle.X += (int)velocity.X;
                this.destRectangle.Y += (int)velocity.Y;

                MoveToPlayer(gameTime);

                base.Update(gameTime, input, game);
            }
        }

        //Enemy podąża za graczem lub wedlug zarzuconego schematu
        public void MoveToPlayer(GameTime gameTime)
        {
            if (hostile)
            {
                this.enemyToPlayer = new Vector2(core.player.destRectangle.X - this.destRectangle.X,
                    core.player.destRectangle.Y - this.destRectangle.Y);

                if (Math.Sqrt(Math.Pow(Math.Abs(enemyToPlayer.X), 2) + Math.Pow(Math.Abs(enemyToPlayer.Y), 2)) < 300)
                {
                    if (enemyToPlayer.X < 0)
                    {
                        this.flip = true;
                        this.velocity.X = -speed;
                    }
                    else
                    {
                        this.flip = false;
                        this.velocity.X = speed;
                    }
                }
                else
                {
                    timer += gameTime.ElapsedGameTime.Milliseconds;
                    if (timer >= 0 && timer < 2000)
                    {
                        this.flip = false;
                        this.velocity.X = speed;
                    }
                    else if (timer >= 2000 && timer < 4000)
                    {
                        this.flip = true;
                        this.velocity.X = -speed;
                    }
                    else if (timer >= 4000)
                        timer = 0;
                }
            }
            else
            {
                timer += gameTime.ElapsedGameTime.Milliseconds;
                if (timer >= 0 && timer < 2000)
                {
                    this.flip = false;
                    this.velocity.X = speed;
                }
                else if (timer >= 2000 && timer < 4000)
                {
                    this.flip = true;
                    this.velocity.X = -speed;
                }
                else if (timer >= 4000)
                    timer = 0;
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (flip)
                spriteBatch.Draw(this.texture, this.destRectangle, null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 0f);
            else
                spriteBatch.Draw(this.texture, this.destRectangle, null, Color.White, 0f, Vector2.Zero, SpriteEffects.FlipHorizontally, 0f);
        }
    }

}