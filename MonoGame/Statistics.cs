﻿using GeonBit.UI;
using GeonBit.UI.Entities;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGame
{
    class Statistics
    {
        private Scale scaleElement = new Scale();

        private PlaySound playSound;
        private Panel panel;
        private Button button_newgame, button_end_game, button_menu;

        public Statistics(Game1 game, Core core, Hud hud)
        {
            // create a panel and position in center of screen
            float panelWidth = (float)(Settings.ScreenWidth * 0.5);
            float panelHeight = (float)(Settings.ScreenHeight * 0.55);

            float scaleValue = (float)scaleElement.GetScale();
            UserInterface.Active.GlobalScale = scaleValue;
            UserInterface.Active.CursorScale = scaleValue;

            panel = new Panel(new Vector2((float)panelWidth / scaleValue, (float)panelHeight / scaleValue), PanelSkin.Default); //Anchor.Center
            UserInterface.Active.AddEntity(panel);

            panel.AddChild(new Header(Text.end_game));

            int hours = (int)(core.timeInPause/1000 / 3600);
            int minutes = (int)(core.timeInPause / 1000 / 60) - hours * 60;
            int seconds = (int)(core.timeInPause / 1000 - hours * 60 - minutes * 60);
            panel.AddChild(new Paragraph(Text.pause_time+": " + hours + "h " + minutes + "m " + seconds + "s", Anchor.AutoCenter));

            hours = (int)(core.timeInGame / 1000 / 3600);
            minutes = (int)(core.timeInGame / 1000 / 60) - hours * 60;
           seconds = (int)(core.timeInGame / 1000 - hours * 60 - minutes * 60);
            panel.AddChild(new Paragraph(Text.game_time + ": " + hours + "h " + minutes + "m " + seconds + "s", Anchor.AutoCenter));
            

            long totalTime = core.timeInPause + core.timeInGame;
            hours = (int)(totalTime / 1000 / 3600);
            minutes = (int)(totalTime / 1000 / 60) - hours * 60;
            seconds = (int)(totalTime / 1000 - hours * 60 - minutes * 60);
            panel.AddChild(new Paragraph(Text.total_time + ": " + hours+"h "+minutes+"m "+seconds+"s", Anchor.AutoCenter));

            int totalOpenedChest = core.goldchestOpened + core.chestOpened;
            panel.AddChild(new Paragraph(Text.chest_opened + ": " + totalOpenedChest, Anchor.AutoCenter));
            panel.AddChild(new Paragraph(Text.item_total + ": " + core.equipment.TotalItems(), Anchor.AutoCenter));

            button_newgame = new Button(Text.new_game, ButtonSkin.Default, Anchor.AutoCenter, size: new Vector2(scaleElement.TextWidth(Text.new_game, 0) / scaleValue, scaleElement.TextHeight(Text.new_game, 0) / scaleValue), offset: new Vector2(0, 10 * scaleElement.GetScaleY()));
            panel.AddChild(button_newgame);

            button_menu = new Button(Text.back_to_menu, ButtonSkin.Default, Anchor.AutoCenter, size: new Vector2(scaleElement.TextWidth(Text.back_to_menu, 0) / scaleValue, scaleElement.TextHeight(Text.back_to_menu, 0) / scaleValue));
            panel.AddChild(button_menu);

            button_end_game = new Button(Text.exit_game, ButtonSkin.Default, Anchor.AutoCenter, size: new Vector2(scaleElement.TextWidth(Text.exit_game, 0) / scaleValue, scaleElement.TextHeight(Text.exit_game, 0) / scaleValue));
            panel.AddChild(button_end_game);

            button_end_game.OnClick = (Entity btn) =>
            {
                playSound = new PlaySound("click", (float)Settings.effectVolume * (float)Settings.masterVolume);
                game.Exit();
            };

            button_menu.OnClick = (Entity btn) =>
            {
                playSound = new PlaySound("click", (float)Settings.effectVolume * (float)Settings.masterVolume);
                panel.Visible = false;
                hud.HideElement();
                game.ChangeScene(Scene.MENU, "");
            };

            button_newgame.OnClick = (Entity btn) =>
            {
                playSound = new PlaySound("click", (float)Settings.effectVolume * (float)Settings.masterVolume);
                panel.Visible = false;
                hud.HideElement();
                game.ChangeScene(Scene.CORE, "");
            };

        }

        public void SetPanelVisible(bool isVisible)
        {
            panel.Visible = isVisible;
        }

    }
}