﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGame
{
    public class Input
    {
        //FIELDS
        MouseState oldMouse;
        MouseState mouse;

        //CONSTRUCTOR
        public Input()
        { }

        public Input(MouseState oldMouse, MouseState mouse)
        {
            this.oldMouse = oldMouse;
            this.mouse = mouse;
        }

        //METHODS
        public bool IsLeftMouseDown()
        {
            return this.mouse.LeftButton == ButtonState.Pressed;
        }

        public bool IsLeftMousePressed()
        {
            return this.oldMouse.LeftButton == ButtonState.Pressed && this.mouse.LeftButton == ButtonState.Released;
        }

        public Point GetMousePosition()
        {
            return new Point(this.mouse.X, this.mouse.Y);
        }
    }
}
