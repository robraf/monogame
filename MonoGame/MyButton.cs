﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGame
{
    public class MyButton
    {
        //FIELDS
        private Texture2D texture;
        private Rectangle destRectangle;
        private Color color;
        public bool isPressed;

        //GETTERS & SETTERS
        public Rectangle Rectangle
        {
            get
            {
                return destRectangle;
            }
        }

        public void SetColor(Color color)
        {
            this.color = color;
        }

        public bool IsPressed()
        {
            bool result = this.isPressed;

            if (this.isPressed)
                this.isPressed = false;

            return this.isPressed;
        }

        //CONSTRUCTOR
        public MyButton(Texture2D texture, int x, int y)
        {
            this.texture = texture;
            this.destRectangle = new Rectangle(x - (texture.Width / 2), y, (int)(this.texture.Width * Settings.PixelRatio), (int)(this.texture.Height * Settings.PixelRatio));
            this.color = Color.White;
            this.isPressed = false;
        }

        public void Update(GameTime gameTime, Input input, Game1 game)
        {
            if (this.Rectangle.Contains(input.GetMousePosition()))
            {
                if (input.IsLeftMousePressed())
                {
                    this.isPressed = true;
                }

                if (input.IsLeftMouseDown())
                {
                    this.SetColor(Color.Gray);
                }
                else
                {
                    this.SetColor(Color.LightGray);
                }
            }
            else
            {
                this.SetColor(Color.White);
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(this.texture, this.destRectangle, this.color);
        }

    }
}
