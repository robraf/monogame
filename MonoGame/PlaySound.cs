﻿using Microsoft.Xna.Framework.Audio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGame
{
    public class PlaySound
    {
        private SoundEffect sound;  
        private SoundEffectInstance soundInstance;

        public PlaySound(string soundName, float volumeValue)
        {
            this.sound = Resources.Sounds[soundName];
            sound.Play(volumeValue, 0, 0);
        } 
    }
}
