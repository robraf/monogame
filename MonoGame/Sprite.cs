﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGame
{
    public enum SpriteType
    {
        OTHER,
        AIR,
        WOOD,
        STONE,
        IRON,
        GOLD,
        DIAMOND,
        BONE,
        HIDE,
        MEAT,
        CHEST,
        GOLDCHEST,
        DOOR,
        SPAWN
    }

    public class Sprite
    {
        //FIELDS
        protected Texture2D texture;
        protected Vector2 position;
        public Rectangle destRectangle;
        private bool isWalkable;
        private int healthPoint;
        private SpriteType type;
        private Vector2 velocity;
        private bool animation;
        private bool appear;
        private bool gravity;
        private int timer;
        private Random random;
        private int direction;
        private Scale scale = new Scale();

        //GETTERS & SETTERS
        public Rectangle Rectangle
        {
            get
            {
                return destRectangle;
            }
        }

        public bool isElementWalkable()
        {
            return this.isWalkable;
        }

        public void hit(int hit)
        {
            this.healthPoint -= hit;
        }

        public int getHP()
        {
            return this.healthPoint;
        }

        public SpriteType getSpriteType()
        {
            return this.type;
        }

        //CONSTRUCTOR
        public Sprite(Texture2D texture, int x, int y) //logo
        {
            this.texture = texture;
            this.destRectangle = new Rectangle(x, y, this.texture.Width, this.texture.Height);
        }

        public Sprite(Texture2D texture, Sprite sprite) //dropping items
        {
            this.texture = texture;
            this.destRectangle = new Rectangle(sprite.Rectangle.Center.X - (this.texture.Width / 2),
                sprite.Rectangle.Center.Y, this.texture.Width, this.texture.Height);
            this.animation = false;
            this.gravity = true;
            this.appear = true;
            this.random = new Random();
            this.direction = random.Next(0, 2);
            this.type = sprite.getSpriteType();
        }

        public Sprite(Texture2D texture, Enemy enemy, int r) //dropping items from monsters
        {
            this.texture = texture;
            this.destRectangle = new Rectangle(enemy.destRectangle.Center.X - (this.texture.Width / 2),
                enemy.destRectangle.Center.Y, this.texture.Width, this.texture.Height);
            this.animation = false;
            this.gravity = true;
            this.appear = true;
            this.random = new Random();
            this.direction = random.Next(0, 2);

            switch (enemy.getEnemyType())
            {
                case EnemyType.PIG:
                    if (r == 1)
                        this.type = SpriteType.MEAT;
                    else if (r == 2)
                        this.type = SpriteType.HIDE;
                    break;
                case EnemyType.SKELETON:
                    this.type = SpriteType.BONE;
                    break;
            }            
        }

        public Sprite(Texture2D texture, Sprite sprite, SpriteType type) //chest drop
        {
            this.texture = texture;
            this.destRectangle = new Rectangle(sprite.Rectangle.Center.X - (this.texture.Width / 2),
                sprite.Rectangle.Center.Y, this.texture.Width, this.texture.Height);
            this.animation = false;
            this.gravity = true;
            this.appear = true;
            this.random = new Random();
            this.direction = random.Next(0, 2);
            this.type = type;
        }

        public Sprite(Texture2D texture, int x, int y, bool walkable, int hp, SpriteType type) //map items
        {
            this.texture = texture;
            this.destRectangle = new Rectangle(x, y, this.texture.Width, this.texture.Height);
            this.isWalkable = walkable;
            this.healthPoint = hp;
            this.type = type;
        }

        //METHODS
        public void SetGravity(bool gravity)
        {
            this.gravity = gravity;
        }



        public bool IsTouchingTop(Sprite sprite)
        {
            return this.Rectangle.Bottom + 2f > sprite.Rectangle.Top &&
                this.Rectangle.Top < sprite.Rectangle.Top &&
                this.Rectangle.Right > sprite.Rectangle.Left &&
                this.Rectangle.Left < sprite.Rectangle.Right;
        }

        //UPDATE & DRAW
        public virtual void Update(GameTime gameTime, Game1 game)
        {
            //POSITION
            if (direction == 0)
                this.destRectangle.X += (int)velocity.X;
            else
                this.destRectangle.X -= (int)velocity.X;
            this.destRectangle.Y += (int)velocity.Y;

            //GRAVITY
            if (gravity)
            {
                this.destRectangle.Y += 3;
            }

            //DROPPING
            if (appear)
            {
                this.destRectangle.Y -= 20;
                this.velocity.Y = -4f;
                this.velocity.X = 1f;
                this.appear = false;
                this.animation = true;
            }

            if (animation)
            {
                float i = 1;
                this.velocity.Y += 0.15f * i;
                this.velocity.X += 0.01f * i;
                this.timer += gameTime.ElapsedGameTime.Milliseconds;
                if (timer >= 500)
                {
                    timer = 0;
                    animation = false;
                }
            }

            if (!animation)
            {
                this.velocity.Y = 0f;
                this.velocity.X = 0f;
            }
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
           // spriteBatch.Draw(this.texture, this.destRectangle, Color.White);

            //skalowanie grafik
            spriteBatch.Draw(this.texture, new Vector2(destRectangle.X, destRectangle.Y), null, Color.White, 0f, Vector2.Zero, scale.GetScale(), SpriteEffects.None, 0f);
        }
    }
}
