﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoGame
{
    public class Camera
    {
        public Matrix Transform { get; private set; }

        public void Follow(GameObject player)
        {
            var position = Matrix.CreateTranslation(-player.destRectangle.X - (player.destRectangle.Width / 2), -player.destRectangle.Y - (player.destRectangle.Height / 2), 0);
            var offset = Matrix.CreateTranslation(Settings.ScreenWidth / 2, Settings.ScreenHeight / 2, 0);
            Transform = position * offset;
        }
    }
}
